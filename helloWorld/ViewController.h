//
//  ViewController.h
//  helloWorld
//
//  Created by Ryan Simpson on 1/22/17.
//  Copyright © 2017 Ryan Simpson. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIView *background;
@property (weak, nonatomic) IBOutlet UIButton *HelloButton;
@property (weak, nonatomic) IBOutlet UILabel *HelloLabel;


@end

