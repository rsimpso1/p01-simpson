//
//  main.m
//  helloWorld
//
//  Created by Ryan Simpson on 1/22/17.
//  Copyright © 2017 Ryan Simpson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
