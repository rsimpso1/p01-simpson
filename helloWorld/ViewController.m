//
//  ViewController.m
//  helloWorld
//
//  Created by Ryan Simpson on 1/22/17.
//  Copyright © 2017 Ryan Simpson. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController
bool clicked = true;
- (IBAction)handleButtonClick:(id)sender {
    if(clicked ==  true){
        [self.HelloButton setTitle:@"Button!" forState:UIControlStateNormal];
        [self.HelloLabel setText:@"Hello Ryan......"];
        //self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"sunrise1.jpeg"]];
        //self.background.backgroundColor = [UIColor blackColor];
    }
    else if(clicked == false){
        //[self.HelloButton setTitle:@"Button...?" forState:UIControlStateNormal];
        [self.HelloLabel setText:@"Hello World"];
    }
    if(clicked == true){
        clicked = false;
    }
    else{
        clicked = true;
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
